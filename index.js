showChannelName();
function createChannelName() {
	let channelName = document.getElementById("channel-name").value;
	fetch('http://localhost:5000/channels', {
			method: 'post',
			body: JSON.stringify({
				name: channelName
			})
		})
		.then(() => {
			showChannelName();
		})
}

function showChannelName() {
	// show channel name
	fetch('http://localhost:5000/channels/')
		.then(function (res) {
			return res.json()
		})
		.then(function (data) {
			let output = "";
			for (let i in data.resources) {
				output +=
					`
						<div class="channel-link">
                        <li>
						 <a href="#" onclick="idShow(${data.resources[i].id})" id="${data.resources[i].id}"  
                            style="color:white;">${data.resources[i].name}</a> 
                        <li><br></div> `;
				document.getElementById("channel").innerHTML = output;
			}
		})
		.catch(function (err) {
			console.log(err)
		})
}

// show messages to his respective channels
channel_id = {}
channelSelected=[]
function idShow(id) {
	var selectChannel=document.getElementById(id);
	channelSelected.push(selectChannel);
	if(channelSelected.length==1){
		channelSelected[0].style.background="grey";
	}
	else{
		channelSelected.shift().style.background="#3a3e98";
		channelSelected[0].style.background="grey";
	}
	let channelId = id;
	channel_id.id = id;
	fetch('http://localhost:5000/messages/')
		.then(function (res) {
			return res.json()
		})
		.then(function (data) {
			console.log(data)
			let output = "";
			let userNameOfChannel = "";
			let val = false;
			for (let i in data.resources) {
				if (data.resources[i].channel_id == channelId) {
					val = true;
					userNameOfChannel = data.resources[i].username;
					output +=
						`<div class="message-text">
                            <i class='fas fa-user' style='font-size:16px'></i>
                            <b>${userNameOfChannel}</b>
                            <li>${data.resources[i].text}</li>
                        </div>`;
					document.getElementById("display-messages").innerHTML = output;
				}
			}
			if (val == false) {
				output +=
					`<div class="message-text">
                        <li>Oops! No messages here.</li>
                    </div>`;
				document.getElementById("display-messages").innerHTML = output;
			}
		})
		.catch(function (err) {
			console.log(err)
		})
}

//creating messages for respective channels and do broadcast using pusher
function createMessages() {
	let userName = prompt("Enter user name...")
	let writeMessage = document.getElementById("write-message").value;
	fetch('http://localhost:5000/messages?channel_id=' + channel_id.id, {
		method: 'post',
		body: JSON.stringify({
			text: writeMessage,
			username: userName,
			channel_id: channel_id.id
		})
	}).then(() => {
		idShow(channel_id.id);
	})
	fetch('http://localhost:5000/broadcast', {
			method: 'post',
			body: JSON.stringify({
				text: writeMessage,
				username: userName,
				channel_id: channel_id.id
			})
		})
		.catch(function (err) {
			console.log(err)
		})
}